package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	public static final int MAX_ROLS =20;
	private int score =0;
	private int[] rolls = new int[20];
	private int currentRoll = 0;

	@Override
	public void roll(int pins) {
		//deve tenere conto dei vecchi tiri 
		rolls[currentRoll++] = pins;

	}

	@Override
	public int score() {
		int score=0;
		for(int currentRoll=0; currentRoll<MAX_ROLS; currentRoll++) {
			if(isStrike(currentRoll)){
				score += rolls[currentRoll+1];
				score += rolls[currentRoll+2];
				
			}
			else if(isSpare(currentRoll)){
				score += rolls[currentRoll+2];
			}
			score += rolls[currentRoll];
			
		}
			
		return score;
		
	}
	
	private boolean isSpare(int currentRoll){
		return currentRoll< 19 && rolls[currentRoll]+rolls[currentRoll+1]== 10  && currentRoll%2 ==0;
	}
	
	private boolean isStrike(int currentRoll){
		return currentRoll< 19 && rolls[currentRoll]== 10  && currentRoll%2 ==0;
	}
	
	

}
